﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task_22
{
    class Program
    {
        static void Main(string[] args)
        {
            var dictionary = new Dictionary<string, string>();
            var fruitlist = new List<string>();
            var vegelist = new List<string>();


            dictionary.Add("banana", "fruit");
            dictionary.Add("cabbage", "vegetable");
            dictionary.Add("carrot", "vegetable");
            dictionary.Add("peach", "fruit");
            dictionary.Add("lettuce", "vegetable");
            dictionary.Add("cauliflower", "vegetable");
            dictionary.Add("nectarine", "fruit");
            dictionary.Add("potato", "vegetable");
            dictionary.Add("apple", "fruit");
            dictionary.Add("sweetcorn", "vegetable");

            foreach (var x in dictionary)
            {
                if (x.Value == "fruit")
                {
                    fruitlist.Add(x.Key);
                }
                else
                {
                    vegelist.Add(x.Key);
                }

            }
            Console.WriteLine($"The fruits are {string.Join(", ", fruitlist)} ");
            Console.WriteLine($"Fruits = {fruitlist.Count()}");
            Console.WriteLine($"Vegetables = {vegelist.Count()}");
        }
    }
}
