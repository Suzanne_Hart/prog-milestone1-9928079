﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task_33
{
    class Program
    {
        static void Main(string[] args)
        {
            var x = 28;
            Double total = 0;
            Console.WriteLine("Please enter number of students for tutorial groups?");
            Double y = Double.Parse (Console.ReadLine());
          
            total = y / x;
            Console.WriteLine($"Your answer is {total} tutorial groups");
        }
    }
}
