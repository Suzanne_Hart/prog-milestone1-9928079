﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task_30
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Please enter a number");
            var input1 = Console.ReadLine();
            Console.WriteLine("Please enter a number");
            var input2 = Console.ReadLine();
            Console.WriteLine($"The total of your two numbers is {input1 + input2}");

            
            int num1 = Convert.ToInt32(input1);
            int num2 = Convert.ToInt32(input2);
            Console.WriteLine($"The total of your two numbers is {num1 + num2}");
        }
    }
}
