﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task_14
{
    class Program
    {
        static void Main(string[] args)
        {
            const double gigabyte = 1023.9606799099;
            const double terabyte = 1;
            var answer = 0.00;
            var total = 0.00;


            Console.WriteLine("Please enter terabytes to convert into gigabytes?");
            answer = double.Parse(Console.ReadLine());
            total = answer * gigabyte;
            Console.WriteLine($"Your answer is {total} gigabytes");

        }
    }
}
