﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task_15
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Let's play a game! You give me five numbers and I will add them all together for you");
            Console.WriteLine("Please enter your first number followed by the enter button");
            var num01 = int.Parse(Console.ReadLine());
            Console.WriteLine("Please enter your second number followed by the enter button");
            var num02 = int.Parse(Console.ReadLine());
            Console.WriteLine("Please enter your third number followed by the enter button");
            var num03 = int.Parse(Console.ReadLine());
            Console.WriteLine("Please enter your fourth number followed by the enter button");
            var num04 = int.Parse(Console.ReadLine());
            Console.WriteLine("Please enter your fifth number followed by the enter button");
            var num05 = int.Parse(Console.ReadLine());
            var result = num01 + num02 + num03 + num04 + num05;

            var numbers = new List<int>();
            numbers.Add(num01);
            numbers.Add(num02);
            numbers.Add(num03);
            numbers.Add(num04);
            numbers.Add(num05);
            Console.WriteLine("You chose the following numbers");
            Console.WriteLine(string.Join(",", numbers));
            
            Console.WriteLine($"The total from adding all of these numbers together is {result}");
        }
    }
}
