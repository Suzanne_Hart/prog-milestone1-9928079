﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task_23
{
    class Program
    {
        static void Main(string[] args)
        {
            var dictionary = new Dictionary<string, string>();
            var weekdaylist = new List<string>();
            var weekendlist = new List<string>();


            dictionary.Add("monday", "weekday");
            dictionary.Add("tuesday", "weekday");
            dictionary.Add("wednesday", "weekday");
            dictionary.Add("thursday", "weekday");
            dictionary.Add("friday", "weekday");
            dictionary.Add("saturday", "weekend");
            dictionary.Add("sunday", "weekend");
            
            foreach (var x in dictionary)
            {
                if (x.Value == "weekday")
                {
                    weekdaylist.Add(x.Key);
                }
                else
                {
                    weekendlist.Add(x.Key);
                }
            }
            Console.WriteLine($"The weekdays are {string.Join(", ", weekdaylist)}");
            Console.WriteLine($"The weekends are {string.Join(", ", weekendlist)}");
        }
    }
}
