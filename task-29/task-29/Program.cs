﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task_29
{
    class Program
    {
        static void Main(string[] args)
        {
            int count;
            string name = "The quick brown fox jumped over the fence";
            count = name.Split(' ').Length;
            Console.WriteLine("The word count is " + count);
            Console.ReadLine();
        }
    }
}
