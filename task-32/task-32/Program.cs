﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task_32
{
    class Program
    {
        static void Main(string[] args)
        {

            var daysofweek = new string[7];

            daysofweek[0] = "Sunday";
            daysofweek[1] = "Monday";
            daysofweek[2] = "Tuesday";
            daysofweek[3] = "Wednesday";
            daysofweek[4] = "Thursday";
            daysofweek[5] = "Friday";
            daysofweek[6] = "Saturday";

            Console.WriteLine("Enter a number between 1 and 7: ");
            int num = Convert.ToInt32(Console.ReadLine());

            if (num == 1)
            {
                Console.WriteLine($"{daysofweek[0] } is Day 1 of the week");
                Console.ReadLine();
            }

            if (num == 2)
            {
                Console.WriteLine($"{daysofweek[1] } is Day 2 of the week");
                Console.ReadLine();
            }

            if (num == 3)
            {
                Console.WriteLine($"{daysofweek[2] } is Day 3 of the week");
                Console.ReadLine();
            }

            if (num == 4)
            {
                Console.WriteLine($"{daysofweek[3] } is Day 4 of the week");
                Console.ReadLine();
            }

            if (num == 5)
            {
                Console.WriteLine($"{daysofweek[4] } is Day 5 of the week");
                Console.ReadLine();
            }

            if (num == 6)
            {
                Console.WriteLine($"{daysofweek[5] } is Day 6 of the week");
                Console.ReadLine();
            }
             
            if (num == 7)
            {
                Console.WriteLine($"{daysofweek[6] } is Day 7 of the week");
                Console.ReadLine();
            }   
        }
    }
}
