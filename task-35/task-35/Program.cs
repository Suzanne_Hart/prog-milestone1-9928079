﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task_35
{
    class Program
    {
        static void Main(string[] args)
        {
            int a;
            int b;
            int c;

            Console.WriteLine("Enter the first number a:");
            a = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Enter the second number b:");
            b = Convert.ToInt32(Console.ReadLine());
            Console.Write("Enter the third number c:");
            c = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine($"The total of a + b = {a + b}");
            Console.WriteLine($"The total of a - b = {a - b}");
            Console.WriteLine($"The total of c x b = {c * b}");
            Console.WriteLine($"The total of b + b = {b + b}");
            Console.WriteLine($"The total of c - b = {c - b}");

            Console.ReadLine();
        }
    }
}
