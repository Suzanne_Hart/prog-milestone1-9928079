﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task_18
{
    class Program
    {
        static void Main(string[] args)
        {
            var names = new List<Tuple<string, string, int>>();
            names.Add(Tuple.Create("Suzanne", "December", 15));
            names.Add(Tuple.Create("Kaleb", "October", 02));
            names.Add(Tuple.Create("Ezra", "March", 16));
            Console.WriteLine(string.Join(",", names));
        }
    }
}
