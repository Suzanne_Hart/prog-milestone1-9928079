﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task_31
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Please enter your number:");
            int num1 = int.Parse(Console.ReadLine());
            bool checkNum3 = (num1 % 3) == 0;
            bool checkNum4 = (num1 % 4) == 0;
            if (num1 == 0)
            {
                checkNum3 = false;
            }
            Console.WriteLine("Divided by 3 and 4? {0}", checkNum3 && checkNum4);
        }
    }
}