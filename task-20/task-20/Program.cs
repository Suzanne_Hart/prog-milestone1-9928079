﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task_20
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] numbers = new int[7];
            numbers[0] = 33;
            numbers[1] = 45;
            numbers[2] = 21;
            numbers[3] = 44;
            numbers[4] = 67;
            numbers[5] = 87;
            numbers[6] = 86;

            for (int i = 0; i < numbers.Length; i++)
            {
                if (numbers[i] % 2 != 0)
                {
                    Console.WriteLine("{0} ", numbers[i]);
                }
            }
        }
    }
}
