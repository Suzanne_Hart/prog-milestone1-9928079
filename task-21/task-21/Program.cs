﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task_21
{
    class Program
    {
        static void Main(string[] args)
        {
            var dictionary = new Dictionary<string, int>();

            dictionary.Add("january", 31);
            dictionary.Add("february", 28);
            dictionary.Add("march", 31);
            dictionary.Add("april", 30);
            dictionary.Add("may", 31);
            dictionary.Add("june", 30);
            dictionary.Add("july", 31);
            dictionary.Add("august", 31);
            dictionary.Add("september", 30);
            dictionary.Add("october", 31);
            dictionary.Add("november", 30);
            dictionary.Add("december", 31);

            foreach (var x in dictionary)
            {
                if (x.Value == 31)
                {
                    Console.WriteLine(x.Key);
                }
            }
        }
    }
}
 

