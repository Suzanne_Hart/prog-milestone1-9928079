﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task_02
{
    class Program
    {
        static void Main(string[] args)
        {
            var month = "";
            var date = "";
            Console.WriteLine("What month of the year were you born?");
            month = Console.ReadLine();
            Console.WriteLine($"Thank you so much, can you also please tell me what date?");
            date = Console.ReadLine();
            Console.WriteLine($"Thanks again! So your birthday is {month} {date}.");
            Console.ReadLine();
        }
    }
}
