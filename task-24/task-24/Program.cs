﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task_24
{
    class Program
    {
        public static void Main(string[] args)
        {
            var menu = "m";

            do
            {
                Console.Clear();

                Console.WriteLine("**********************************");
                Console.WriteLine("  What is your favourite colour?  ");
                Console.WriteLine("                                  ");
                Console.WriteLine("           1. Blue                ");
                Console.WriteLine("           2. Black               ");
                Console.WriteLine("           3. Red                 ");
                Console.WriteLine("           4. Yellow              ");
                Console.WriteLine("                                  ");
                Console.WriteLine("                                  ");
                Console.WriteLine("**********************************");
                Console.WriteLine();
                Console.WriteLine("Please select an option");
                menu = Console.ReadLine();

                if (menu == "1")
                {
                    Console.Clear();
                    Console.WriteLine("Your favourite colour is Blue!");
                    Console.WriteLine($"Press m to return to main menu");
                    menu = Console.ReadLine();
                }

                if (menu == "2")
                {
                    Console.Clear();
                    Console.WriteLine("Your favourite colour is Black!");
                    Console.WriteLine("Press m to return to main menu");
                    menu = Console.ReadLine();
                }

                if (menu == "3")
                {
                    Console.Clear();
                    Console.WriteLine("Your favourite colour is Red!");
                    Console.WriteLine("Press m to return to main menu");
                    menu = Console.ReadLine();
                }

                if (menu == "4")
                {
                    Console.Clear();
                    Console.WriteLine("Your favourite colour is Yellow!");
                    Console.WriteLine("Press m to return to main menu");
                    menu = Console.ReadLine();
                }

            } while (menu == "m");

      
        }
    }
}